import express, { Request, Response } from "express"
import { addProduct, findById, findAll, deleteById, updateProduct } from "./dao"

const router = express.Router()

router.post('/', async (req: Request, res: Response) => {
    const { name, price} = req.body
    const id = await addProduct(name, price)
    res.send({id, name, price})
})

router.get('/:id', async (req: Request, res: Response) => {
    const result = await findById(Number(req.params.id))
    res.send(result) 
})

router.get('/', async (req: Request, res: Response) => {
    const result = await findAll()
    res.send(result) 
})

router.delete('/:id', async (req: Request, res: Response) => {
    await deleteById(Number(req.params.id))
    res.status(200).send() 
})

router.put('/:id', async (req: Request, res: Response) => {
    const { name, price} = req.body
    await updateProduct(Number(req.params.id), name, price)
    res.send() 
})



export default router
