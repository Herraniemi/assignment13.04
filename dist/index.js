"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const db_1 = require("./db");
const server = (0, express_1.default)();
(0, db_1.createProductsTable)();
const { PORT } = process.env;
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT);
});
